%% bi_level_OED
% Function bi_level_OED solves bi-level optimization problems of A-optimal design formulated in 
% following way:
%
% $$
% \begin{array}{cl}
% \min\limits_{\textbf{u}} & \sum_{i}^{n_p} (p^{U}_{i}-p^{L}_{i})\\
% &\max\limits_{\textbf{p}} \sum_{i}^{n_p} (p^{U}_{i}-p^{L}_{i})\\
% & \quad \quad s.t. \ \sum_{k=1}^{n} (y_k-y^m_k)^2\sigma^{-2} \leq \chi^2_{\alpha,np}\end{array}
% $$
% 
% 
% where minimization is a general semi infinite problem or GSIP and
% maximization is a lower level problem or LLP. Variables *u* are a control vector variables
% belonging to the upper level problem (GSIP) and *p* are parameters variables belonging 
% to the lower level problem (LLP). $\sum_{i}^{n_p} (p^{U,i}_{i}-p^{L,i}_{i})$ is
% sum of differences between upper values of parameters and lower values of
% parameters. $\sum_{k=1}^{n} (y_k-y^m_k)^2\sigma^{-2}$ is a function of
% affiliation, this function decides which values of parameters are
% acceptable based on $\chi^2$ distribution. $n_p$ is number of parameters.
% $\sigma$ is standard deviation of measurement noise. $y_k$ is modeled
% output in k-th step for discrete models and in k-th time for static models
% based on parameters estimated by solver and $y^m_k$ is output based on
% user defined parameters.
%
% bi_level_OED offer two strategies for solving these problems:
%
% - KKT-based approach (KKT)
%
% - Mitsos and Tsoukalas algorithm (MT)
%
% KKT - based approach is experimental and may fail.
%
%% Syntax
% [J_opt, U_opt, Param_opt, Log] = bi_level_OED(model_x, model_y, u,
% parameters, parameters_values, x0, x0_value, lb, ub, start, model_type, strategy,
% 'MaxIter', MaxIter, 'Alpha', Alpha, 'Sigma', Sigma, 'Tol', Tol, 'Xi', Xi
% 'Gl_max', Gl_max, 'Gu_max', Gu_max, 'Baronset', Baronset)
%% Inputs definition
% model_x => Model of states when discrete model is used x0 as in 
% initial condition must be part of model(symbolic function).
%
% model_y => Model of outputs symbolic function model x must be part of model. (symbolic function)
%
% u => Vector of inputs. Decides number of steps for discrete model and number of measurements for static model. (symbolic column vector)
%
% parameters =>  Symbolic row vector of parameters used in model.
%
% parameters_values => Parameters values which will be used to simulate
% output measurement. Parameter values are link to their corresponding name
% based on order of entry. (row vector)
%
% x0 => Symbolic representation of initial condition. (symbolic scalar)
%
% x0_value => Value of initial condition. (number)
%
% lb => Lower bounds for parameters and control vector in the form [lb_u,lb_parameter1,lb_parameter2,...,lb_parameter(last)]. (row vector)
%
% ub => Upper bounds for parameters and control vector in the form [ub_u,ub_parameter1,ub_parameter2,...,ub_parameter(last)]. (row vector)
%
% start => Define starting values for parameters (MT)
% format:
%   [parameter1_low parameter_2    ... parameter_last     ]
%   [parameter1_up  parameter_2    ... parameter_last     ]
%   [parameter1     parameter2_low ... parameter_last     ]
%   [parameter1     parameter2_up  ... parameter_last     ]
%   [     .               .                  .            ] 
%   [     .                  .               .            ]   
%   [     .                      .           .            ]
%   [     .                         .        .            ]
%   [parameter1     parameter2     ... parameter_last_low ]
%   [parameter1     parameter2     ... parameter_last_up  ]
%
% model_type => Type of model "Static" or "Discrete". (string)
%
% strategy => Type of algorithm used "KKT" or "MT". (string)
%
% MaxIter => Maximal number of iteration, default is 50. (MT) (positive number)
%
% Alpha => Probability value used by function chi2inv to generate
%          $\chi^2_{\alpha,np}$. Number between 0 and 1.
%
% Sigma => Standard deviation of the measurement noise. (positive number)
%
% Tol => Strictness of end condition for MT algorithm. (small positive number)
%
% Xi  => Coefficient for MT strategy in every iteration with possible value (0,1),
%        authors recommend value 0.5.(row vector with length of MaxIter)
%
% Gl_max => Overestimate of maximal positive value of LLP constraints (MT) (positive number).
% 
% Gu_max => Overestimate of maximal positive value of LLP objective function (MT) (positive number).
%
% BarRuntime = > Vector of maximal BARON runtimes in seconds in individual
% iterations. To be correctly interpreted there must be one value for 
% every iteration, default number of iterations is 30. ( row vector)
%
% Baronset => Baronset setup (for more information type baronset). It is
% strongly advised to give solver (Baron) enough time to run its
% calculations, it could mean in some cases setting 'MaxTime' value as
% high as 10 800. (3 hours per iteration)
% 
% 
%% Outputs definition
% J_opt is optimal value of GSIP objective function
%
% U_opt is optimal value of control input vector
%
% Param_opt is optimal value of parameters in format:
% [parameter1_low, parameter2_low ... parameter_low_last ]
% [parameter1_up , parameter2_up  ... parameter_up_last  ]
%
% Log is a structure of variables with additional information:
% 
% * Log acquired using KKT strategy gives Exitflag from Baron and vector of
% KKT - multipliers where fist multiplier belong to first constraint,
% second multiplier belongs to second constraint and so on.
% * Log acquired using MT strategy gives variable LastIter which shows 
% number of iterations taken, structure LBD which has results of GSIP, LLP
% and LLP-AUX from every iteration for Lower bounding scheme and UBD which 
% has results of GSIP, LLP from every iteration for upper bounding scheme.
% 
%% Example for static model
% Define symbolic variables
syms p1 p2 x0

% Decide number of steps by choosing length of control vector u 
% (column vector)
u = sym('u',[4 1]);

% Define model_x
model_x = 1-exp(-p2*u(1));

% Define model_y
model_y = p1*model_x;

% Define model parameters (result will be displayed in this order)
% (row vector)
parameters = [p1,p2];

% Define values of parameters in the model
% (values are linked to parameters names based on order)
% (row vector)
parameters_values = [2.5,0.5];

% Define the value of initial condition
x0_value = 0;

% Define lb - lower bounds for parameters and control vector
% form lb=[lb_u,lb_parameter1,lb_parameter2,...,lb_parameter(last)]
% (row vector)
lb = [0 0 0];

% Define ub - upper bounds for parameters and control vector
% form ub=[ub_u,ub_parameter1,ub_parameter2,...,ub_parameter(last)]
% (row vector)
ub = [20 10 10];

% Define starting values for parameters (MT)
% format:
%   [parameter1_low parameter_2    ... parameter_last     ]
%   [parameter1_up  parameter_2    ... parameter_last     ]
%   [parameter1     parameter2_low ... parameter_last     ]
%   [parameter1     parameter2_up  ... parameter_last     ]
%   [     .               .                  .            ] 
%   [     .                  .               .            ]               
%   [     .                      .           .            ]
%   [     .                         .        .            ]
%   [parameter1     parameter2     ... parameter_last_low ]
%   [parameter1     parameter2     ... parameter_last_up  ]
start = [0.1 0.1
         10 0.1
         0.1 0.1
         0.1 10];

% Choose between "static" and "discrete" models
model_type = "static";

% Maximal number of iteration (MT and BF)
MaxIter  = 10;

% Alpha is the probability in given to 
% Chi-square inverse cumulative distribution function chi2inv
Alpha    = 0.95;

% Standard deviation of the measurement noise
Sigma    = 0.1;

% Strictness of end condition for MT and BF
Tol      = 1e-4;

% Coefficient for MT => LLP AUX in every cycle (MT)
% vector with length of MaxIter
% Its recomended to start with value of 0.5 and gradually increase it
% to value of 0.9
Xi       = linspace(0.68,0.9,30);

% Overestimate of maximal positive value of lower level constraints (MT)
Gl_max   = 5;

% Overestimate of maximal positive value of obj_LLP (MT)
Gu_max   = 20;

% Baron runtime in every iteration, if not set Baronset value is
% used. Vector with length of MaxIter(Default 50). Elements of this 
% vector corespond to the maximal BARON runtime(in seconds) in given cycle
% (row vector)
BarRuntime = [ones(1,8)*400,ones(1,10)*800,ones(1,12)*1200];

% Baronset setup, see Baronset
Baronset = baronset('MaxTime',1800,'threads',8);
%% Results acquired using all strategies
% Available strategies are "KKT" or "MT"

for strategy = ["KKT","MT"]
disp(strategy)
[J_opt, U_opt, Param_opt, Log] = bi_level_OED(model_x,model_y,u,parameters,...>
    parameters_values,x0,x0_value,lb,ub,start,model_type,strategy,...
    'MaxIter',MaxIter,'Alpha',Alpha,'Sigma',Sigma,'Tol',Tol,'Xi',Xi,...
    'Gl_max',Gl_max,'Gu_max',Gu_max,'BarRuntime',BarRuntime,...
    'Baronset',Baronset)
end

