function [vars_order,obj,nlcon,cl,cu]=CreateNlconKKT(obj_fun,obj_LLP,const,up_vars,lo_vars)
% Define multipliers
lambda = sym('lambda',[length(const) 1]);

% Define nlcon as vector consisting of lagrangian gradient,complementary
% slacknes, primal and dual feasibility conditions
Lag_grad = transpose(jacobian(-1*obj_LLP+transpose(lambda)*const,lo_vars));
nlcon    = [Lag_grad;lambda.*const;const;-1*lambda(:)];

% save dimensions
len_eq   = length([Lag_grad;lambda.*const;]);
len_ineq = length([const;lambda(:)]);

% Format variables for function handle nlcon. 
% Desired output[up_vars,lo_vars,KKT_multipliers]
vars_order = [up_vars(:);lo_vars(:);lambda(:)];

% Create bounds for constraints
cl    = [zeros(len_eq,1);ones(len_ineq,1)*-inf];
cu    = zeros(length(nlcon),1);

% Create constraints
nlcon = matlabFunction(nlcon,'Vars',{vars_order});

% Create objective function
obj   = matlabFunction(obj_fun,'Vars',{vars_order});
end