function [J_opt, U_opt, Param_opt, Log] = bi_level_OED(model_x, model_y, u, parameters, parameters_values, x0, x0_value, lb, ub, start, model_type, strategy, varargin)
% BI_LEVEL_OED solves problem of optimal experiment design using
%              KKT- based approach (KKT) and Mitsos and Tsoukalas algorithm(MT).
%
%   Calling form:
%
%       [J_opt, U_opt, Param_opt, Log] = bi_level_OED(model_x, model_y, u,...
%       parameters, parameters_values, x0, x0_value, lb, ub, start,...
%       model_type, strategy, varargin)
%
%   Possible additional arguments
%       ['MaxIter','Alpha','Sigma','Tol','Xi','Gl_max','Gu_max','Baronset']
%
% Example for static model
%
% Define symbolic variables
% syms p1 p2 x0
%
% Decide number of steps by choosing length of control vector u (column vector)
% u = sym('u',[4 1]);
%
% Define model_x
% model_x = 1-exp(-p2*u(1));
%
% Define model_y
% model_y = p1*model_x;
%
% Define model parameters (result will be displayed in this order) (row vector)
% parameters = [p1,p2];
%
% Define values of parameters in the model
% (values are linked to parameters names based on order) (row vector)
% parameters_values = [2.5,0.5];
%
% Define the value of initial condition
% x0_value = 0;
%
% Define lb - lower bounds for parameters and control vector
% form lb=[lb_u,lb_parameter1,lb_parameter2,...,lb_parameter(last)] (row vector)
% lb = [0 0 0];
%
% Define ub - upper bounds for parameters and control vector
% form ub=[ub_u,ub_parameter1,ub_parameter2,...,ub_parameter(last)] (row vector)
% ub = [20 10 10];
%
% Define starting values for parameters (MT)
% format:
%   [parameter1_low parameter2     ... parameter_last     ]
%   [parameter1_up  parameter2     ... parameter_last     ]
%   [parameter1     parameter2_low ... parameter_last     ]
%   [parameter1     parameter2_up  ... parameter_last     ]
%   [     .               .                  .            ]
%   [     .                  .               .            ]
%   [     .                      .           .            ]
%   [     .                         .        .            ]
%   [parameter1     parameter2     ... parameter_last_low ]
%   [parameter1     parameter2     ... parameter_last_up  ]
% start = [0 0
%          10 0
%          0 0
%          10 0];
%
% Choose between "static" and "discrete" models
% model_type = "static";
%
% Maximal number of iteration (MT and BF)
% MaxIter  = 30;
%
% Alpha is the probability in given to
% Chi-square inverse cumulative distribution function chi2inv
% Alpha    = 0.6;
%
% Standard deviation of the measurement noise
% Sigma    = 0.1;
%
% Strictness of end condition for MT and BF
% Tol      = 1e-4;
%
% Coefficient for MT strategy with possible value <0,1> in every iteration
% Authors recommend Xi = 0.5
% (row vector with length of MaxIter)
% Xi       = linspace(0.5,0.9,30);
%
% Overestimate of maximal positive value of lower level constraints (MT)
% Gl_max   = 5;
%
% Overestimate of maximal positive value of obj_LLP (MT)
% Gu_max   = 20;
%
% Baron runtime in every iteration, if not set Baronset value is
% used. Vector with length of MaxIter(Default 50). Elements of this 
% vector corespond to the maximal BARON runtime(in seconds) in given cycle
% (row vector)
% BarRuntime = [ones(1,8)*200,ones(1,10)*600,ones(1,12)*900];
%
% Baronset setup, see Baronset
% Baronset = baronset('MaxTime',10800,'PrLevel',0,'threads',8);
%
% See also bi_level baronset baron


% Prepare to validation functions
ValidSym       = @(x) isscalar(x) && class(x)=="sym";
ValidSymCol    = @(x) iscolumn(x) && class(x)=="sym";
ValidSymRow    = @(x) isrow(x)    && class(x)=="sym";
ValidMat       = @(x) ismatrix(x) && isnumeric(x);
ValidRow       = @(x) isrow(x)    && isnumeric(x);
ValidNumber    = @(x) isscalar(x) && isnumeric(x);
ValidPosNumber = @(x) isscalar(x) && isnumeric(x) && x>0 ;
ValidModelType = @(x) lower(x) == "static" | lower(x)=="discrete";
ValidStrategy  = @(x) upper(x)=="KKT" || upper(x)=="MT";
ValidAlpha     = @(x) isscalar(x) && isnumeric(x) && x>0 && x<1;

p = inputParser;
% Define required inputs
addRequired(p,'model_x',ValidSym);
addRequired(p,'model_y',ValidSym);
addRequired(p,'u',ValidSymCol);
addRequired(p,'parameters',ValidSymRow);
addRequired(p,'parameters_values',ValidRow)
addRequired(p,'x0',ValidSym);
addRequired(p,'x0_value',ValidNumber);
addRequired(p,'lb',ValidRow);
addRequired(p,'ub',ValidRow);
addRequired(p,'start',ValidMat);
addRequired(p,'model_type',ValidModelType);
addRequired(p,'strategy',ValidStrategy );


% Define optinal inputs
addOptional(p,'MaxIter',30,ValidPosNumber);
addOptional(p,'Alpha',0.95,ValidAlpha);
addOptional(p,'Sigma',0.1,ValidPosNumber);
addOptional(p,'Tol',1e-3,ValidPosNumber);
addOptional(p,'Xi',[0.75],ValidRow);
addOptional(p,'Gl_max',30,ValidPosNumber);
addOptional(p,'Gu_max',35,ValidPosNumber);
addOptional(p,'BarRuntime',[300],ValidRow)
addOptional(p,'Baronset',baronset);
% Parse
parse(p,model_x,model_y,u,parameters,parameters_values,x0,x0_value,...
    lb,ub,start,model_type,strategy,varargin{:});

if p.Results.strategy == "KKT"
    [J_opt,U_opt,Param_opt,Log]=bi_level_KKT_OED(p.Results.model_x,...
        p.Results.model_y,p.Results.u,p.Results.parameters,...
        p.Results.parameters_values,p.Results.x0,p.Results.x0_value,...
        p.Results.lb,p.Results.ub,p.Results.model_type,'Alpha',...
        p.Results.Alpha,'Sigma',p.Results.Sigma,'Baronset',...
        p.Results.Baronset);

elseif p.Results.strategy == "MT"
    [J_opt,U_opt,Param_opt,Log]=bi_level_MT_OED(p.Results.model_x,...
        p.Results.model_y,p.Results.u,p.Results.parameters,...
        p.Results.parameters_values,p.Results.x0,p.Results.x0_value,...
        p.Results.lb,p.Results.ub,p.Results.start,p.Results.model_type,...
        'MaxIter',p.Results.MaxIter,'Alpha',p.Results.Alpha,...
        'Sigma',p.Results.Sigma,'Tol',p.Results.Tol,'Xi',p.Results.Xi,...
        'Gl_max',p.Results.Gl_max,'Gu_max',p.Results.Gu_max,...
        'BarRuntime',p.Results.BarRuntime,'Baronset',p.Results.Baronset);
end
end
