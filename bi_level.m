function [J_opt, lv_opt, uv_opt, Log] = bi_level(obj_fun, obj_LLP, const, lv, uv, lb, ub, start, strategy, varargin)
% BI_LEVEL solves bi-level optimization problem utilizing BARON solver and
%		   Blankenship & Falks algorithm (BF) or using reformulation of lower
%		   level problem trough KKT-conditions (KKT) or Mitsos & Tsoukalas
%		   algorithm (MT)
%
%	Calling form:
%
%	[J_opt, lv_opt, uv_opt, Log] = ...
%	bi_level(obj_fun, obj_LLP, const, lv, uv, lb, ub, start, strategy, varargin)
%
% 	Possible additional arguments:
%   ['MaxIter','Tol','Xi','Gl_max','Gu_max','Eps_L','Eps_U','Baronset']
%
%   Program assumes following form of optimization problem:
%       min obj_fun(x,y)
%        x
%           max obj_LLP(x,y)
%            y
%            s.t. const(x,y)<=0
%	Where
%
%   obj_fun is objective function for upper level problem,
%           defined in symbolic toolbox.
%   obj_LLP is objective function for lower level problem,
%           defined in symbolic toolbox.
%	const is a column vector of LLP constraints.
%
% 	Example of inputs
% 	syms x y v w
%
%   Objective function for upper level problem (symbolic function)
%   obj_fun  = y;
%
%   Objective function lower level problem or LLP(symbolic function)
%   obj_LLP  = (x+v)^2-(y+w);
%
%   Constraints for lower level problem(symbolic column vector)
%   const    = [[v,w]*inv([0.8 -0.6;-0.6 0.8])*[v;w]-1];
%
%   Lower and upper bounds form [uv(1),uv(2),...,uv(n),lv(1),lv(2),...,lv(m)]
%	(row vector)
%   lb       = [-10 -10 -10 -10];
%   ub       = [10 10 10 10];
%
%   Upper level problem variables (symbolic column vector)
%   uv       = [x;y];
%
%   Lower level problem variables (symbolic column vector)
%   lv       = [v;w];
%
%   Starting position of lower level variables for MT and BT strategy
%	(column vector)
%   start    = [0;0];
%
%   Example of my Baronset setup (for more information type baronset)
%   Baronset = baronset('MaxTime',150,'PrLevel',0,'LPSol',3,'threads',8);
%
%   Strictness of end condition for MT and BF strategies (small positive number)
%   Tol      = 1e-4;
%
%   Coefficient for MT strategy with possible value <0,1> in every iteration
%   Authors recommend Xi = 0.5
%   (row vector with length of MaxIter)
%   Xi       = linspace(0.5,0.9,30);
%
%   Overestimate of maximal positive value of lower level constraints (MT)
%	(positive number)
%   Gl_max   = 25;
%
%   Overestimate of maximal positive value of obj_LLP (MT) (positive number)
%   Gu_max   = 20;
%
%   Positive value of restriction of right hand side of constraints (MT)
%   (positive number)
%   Eps_L    = 1.5;
%   Eps_U    = 1;
%
%   Maximal number of iteration (MT and BF) (positive number)
%   MaxIter  = 30;
%
%   Baron runtime in every iteration, if not set Baronset value is
%   used. Vector with length of MaxIter(Default 50). Elements of this 
%   vector corespond to the maximal BARON runtime(in seconds) in given cycle
%   (row vector)
%   BarRuntime = [ones(1,8)*400,ones(1,10)*1800,ones(1,12)*3600];
%
%   See also bi_level_OED baronset baron

% Prepare to validation functions
ValidSym       = @(x) isscalar(x) && class(x)=="sym";
ValidSymCol    = @(x) iscolumn(x) && class(x)=="sym";
ValidCol       = @(x) iscolumn(x) && isnumeric(x);
ValidRow       = @(x) isrow(x)    && isnumeric(x);
ValidNumber    = @(x) isscalar(x) && isnumeric(x) && x>0;
ValidStrategy  = @(x) x=="KKT" || x=="MT" || x=="BF";


p = inputParser;
% Define required inputs
addRequired(p,'obj_fun',ValidSym);
addRequired(p,'obj_LLP',ValidSym);
addRequired(p,'const',ValidSymCol);
addRequired(p,'uv',ValidSymCol);
addRequired(p,'lv',ValidSymCol);
addRequired(p,'lb',ValidRow);
addRequired(p,'ub',ValidRow);
addRequired(p,'start',ValidCol);
addRequired(p,'strategy',ValidStrategy);

% Define optinal inputs
addOptional(p,'MaxIter',30,ValidNumber);
addOptional(p,'Tol',1e-6,ValidNumber);
addOptional(p,'Xi',[0.7],ValidRow);
addOptional(p,'Gl_max',30,ValidNumber);
addOptional(p,'Gu_max',35,ValidNumber);
addOptional(p,'Eps_L',2,ValidNumber);
addOptional(p,'Eps_U',2,ValidNumber);
addOptional(p,'Eps_R',2,ValidNumber);
addOptional(p,'BarRuntime',[300],ValidRow)
addOptional(p,'Baronset',baronset);
% Parse
parse(p,obj_fun,obj_LLP,const,uv,lv,lb,ub,start,strategy,varargin{:});

if p.Results.strategy == "BF"
    [J_opt,lv_opt,uv_opt,Log] = bi_level_BF(p.Results.obj_fun,...
        p.Results.obj_LLP,p.Results.const,p.Results.uv,p.Results.lv,...
        p.Results.lb,p.Results.ub,p.Results.start,'MaxIter',...
        p.Results.MaxIter,'Tol',p.Results.Tol,'Baronset',p.Results.Baronset);

elseif p.Results.strategy == "MT"
    [J_opt,lv_opt,uv_opt,Log] = bi_level_MT(p.Results.obj_fun,...
        p.Results.obj_LLP,p.Results.const,p.Results.uv,p.Results.lv,...
        p.Results.lb,p.Results.ub,p.Results.start,...
        'MaxIter',p.Results.MaxIter,'Tol',p.Results.Tol,...
        'Xi',p.Results.Xi,'Gl_max',p.Results.Gl_max,...
        'Gu_max',p.Results.Gu_max,'Eps_L',p.Results.Eps_L,...
        'Eps_R',p.Results.Eps_R,'Eps_U',p.Results.Eps_U,...
        'BarRuntime',p.Results.BarRuntime,'Baronset',p.Results.Baronset);

elseif p.Results.strategy == "KKT"
    [J_opt,lv_opt,uv_opt,Log] = bi_level_KKT(p.Results.obj_fun,...
        p.Results.obj_LLP,p.Results.const,p.Results.uv,p.Results.lv,...
        p.Results.lb,p.Results.ub,'Baronset',p.Results.Baronset);
end
end
