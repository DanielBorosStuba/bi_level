%% Bi_level
% Function bi_level solves bi-level optimization problems formulated in 
% following way:
%
% $$
% \begin{array}{cl}
% \min\limits_{\textbf{x}} & f(\textbf{x,y})\\
% &\max\limits_{\textbf{y}} g_{u}(\textbf{x,y})\\
% & \quad \quad s.t. \ g_{l}(\textbf{x,y}) \ \leq 0\end{array}
% $$
% 
% where minimization is a general semi infinite problem or GSIP and
% maximization is a lower level problem or LLP. Variables *x* are variables
% belonging to the upper level problem (GSIP) and *y* are variables belonging 
% to the lower level problem (LLP). $f(\textbf{x,y})$ is objective function of
% GSIP, $g_u (\textbf{x,y})$ is objective function of LLP and
% $g_l(\textbf{x,y})$ are constraints for LLP
%
% bi_level offer three strategies for solving these problems:
%
% - KKT-based approach (KKT)
% 
% - Blankenship and Falks algorithm (BF)
%
% - Mitsos and Tsoukalas algorithm (MT)
%
% KKT - based approach is experimental and may fail.
%% Syntax
% [J_opt, lv_opt, uv_opt, Log] = bi_level(obj_fun, obj_LLP, const, lv, uv, lb, ub,
% start, strategy, 'MaxIter', MaxIter, 'Tol', Tol, 'Xi', Xi, 'Gl_max', Gl_max,  
% 'Gu_max', Gu_max, 'Eps_L', Eps_L, 'Eps_R', Eps_R ,'Eps_U' ,Eps_U, 'Baronset', Baronset)
%
%% Inputs definition
% obj_fun      => Objective function for GSIP (symbolic function).
%
% obj_LLP      => Objective function LLP (symbolic function).
%
% const        => Constraints for lower level problem (symbolic column vector).
%
% lv           => LLP variables (symbolic column vector).
%
% uv           => GSIP variables (symbolic column vector).
%
% lb,ub        => Lower and upper bounds in the form
% [uv(1),uv(2),...,uv(n),lv(1),lv(2),...,lv(m)] (row vectors).
%
% start        => Starting position of lower level variables for MT and BT strategy (column vector).
%
% strategy     => which algorithm to use "KKT" or "BF" or "MT"(string).
%
% Baronset     => Baronset setup (for more information type baronset).
%
% Tol          => Strictness of end condition for MT and BF strategies(small positive number).
%
% Xi           => Coefficient for MT strategy in every iteration with possible value (0,1),
%                 authors recommend value 0.5.(row vector with length of MaxIter)
%
% Gl_max       => Overestimate of maximal positive value of LLP constraints (MT) (positive number).
%
% Gu_max       => Overestimate of maximal positive value of LLP objective function (MT) (positive number).
%
% Eps_L, Eps_U => Positive value of restriction of right hand side of constraints (MT) (positive number).
%
% MaxIter      => Maximal number of iteration (MT and BF)(positive number).
% 
% BarRuntime   => Maximal runtime of BARON in every iteration (MT and
%                 BF)(row vector).
%% Outputs definition
% J_opt is optimal value of GSIP objective function
%
% lv_opt is a vector of LLP variables in the same format as was given
% through lv
%
% uv_opt is a vector of GSIP variables in the same format as was given
% through uv
%
% Log is a structure of variables with additional information:
% 
% * Log acquired using BF strategy gives Exitflag for GSIP, LLP and variable 
% LastIter which shows number of iterations taken.
% * Log acquired using KKT strategy gives Exitflag from Baron and vector of
% KKT - multipliers where fist multiplier belong to first constraint,
% second multiplier belongs to second constraint and so on.
% * Log acquired using MT strategy gives variable LastIter which shows 
% number of iterations taken, structure LBD which has results of GSIP, LLP
% and LLP-AUX from every iteration for Lower bounding scheme and UBD which 
% has results of GSIP, LLP from every iteration for upper bounding scheme.
%
%% Example
syms x y v w
% Objective function for upper level problem
% (symbolic function)
obj_fun  = y;
% Objective function lower level problem or LLP
% (symbolic function)
obj_LLP  = (x+v)^2-(y+w);
% Constraints for lower level problem 
% (symbolic column vector)
const    = [[v,w]*inv([0.8 -0.6;-0.6 0.8])*[v;w]-1];
% Lower and upper bounds 
% form [uv(1),uv(2),...,uv(n),lv(1),lv(2),...,lv(m)]
% (row vector)
lb       = [-10 -10 -10 -10];
ub       = [10 10 10 10];
% Upper level problem variables
% (symbolic column vector)
uv       = [x;y];
% Lower level problem variables
% (symbolic column vector)
lv       = [v;w];
% Starting position of lower level variables for MT and BT strategy
% (column vector)
start    = [0;0];
% Example of my Baronset setup 
% (for more information type baronset) 
Baronset = baronset('MaxTime',1800,'threads',8);
% Strictness of end condition for MT and BF strategies
% (small positive number)
Tol      = 1e-4;
% Coefficient for MT strategy with possible value <0,1> in every iteration
% Authors recommend Xi = 0.5
% (row vector with length of MaxIter)
Xi       = linspace(0.5,0.9,30);
% Overestimate of maximal positive value of lower level constraints (MT)
% (positive number)
Gl_max   = 25;
% Overestimate of maximal positive value of obj_LLP (MT)
% (positive number)
Gu_max   = 20;
% Positive value of restriction of right hand side of constraints (MT)
% (positive number)
Eps_L    = 1.5;
Eps_U    = 1;
% Maximal number of iteration (MT and BF)
% (positive number)
MaxIter  = 30;
% Baron runtime in every iteration, if not set Baronset value is
% used. Vector with length of MaxIter(Default 50). Elements of this 
% vector corespond to the maximal BARON runtime(in seconds) in given cycle
% (row vector)
BarRuntime = [ones(1,8)*400,ones(1,10)*1800,ones(1,12)*3600];


%% Results acquired using all three strategies
% Available strategies are "BF" or "KKT" or "MT"
for strategy = ["KKT","BF","MT"]
    disp(strategy)
    [J_opt,lv_opt,uv_opt,Log] = bi_level(obj_fun,obj_LLP,const,lv,uv,lb,ub...
        ,start,strategy,'MaxIter',MaxIter,'Tol',Tol,'Xi',Xi,'Gl_max',...
        Gl_max,'Gu_max',Gu_max,'Eps_L',Eps_L,'Eps_R',2,'Eps_U',Eps_U,...
        'BarRuntime',BarRuntime,'Baronset',Baronset)
end
 

