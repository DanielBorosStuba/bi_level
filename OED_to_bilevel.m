function [obj_fun,obj_LLP,const,L_bounds,U_bounds,lo_vars,up_vars]= OED_to_bilevel(model_x,model_y,u,parameters,parameters_values,x0,x0_value,lower_bounds,upper_bounds,alfa,sigma,ModelType)

% Create a matrix of parameters
param_mat = sym('parameter',[length(parameters)*2 length(parameters)]);

% Initialize a symbolic vector for constraints 
const     = sym('func',[2*length(parameters) 1]);

% Initialize a symbolic vector for objective function
obj_fun   = sym('obj_func',[length(parameters) 1]);

% Initialize variable for upper level problem and lower level problem
up_vars   = u;
lo_vars   = sym('lo_vars',[2*length(parameters)^2 1]);

% Initialize vectors for lower and upper bounds
U_bounds  = ones(1,2*length(parameters).^2 + length(u));
L_bounds  = ones(1,2*length(parameters).^2 + length(u));

% Initialize symbolic vector for constriants
Z         = sym('Z',[length(u) 1]);

% Create equations for desired number of steps
if lower(ModelType) == "static"
    Z(1)        = model_y;
    for i       = 1:length(u) - 1
        Z(i+1)  = subs(model_y,u(1),u(i+1));
        
    end
elseif lower(ModelType) == "discrete"
    Z(1)        = model_y;
    A           = model_x;
    for i       = 1:length(u) - 1
        Z(i+1)  = subs(subs(model_y,u(1),u(i+1)),x0,A);
        A       = subs(subs(model_x,u(1),u(i+1)),x0,A);
    end
end
% Create experiment model
Y_model   = subs(Z,parameters,parameters_values);

% Compute Chi squared
Chi       = chi2inv(alfa,length(parameters));

% Creates constraints
for i = 1:1:2*length(parameters)
    const(i) = sum((Y_model - subs(Z,parameters,param_mat(i,:))).^2)*sigma^-2 - Chi;
end
const        = subs(const,x0,x0_value);

% Create Objective function
for i = 1:1:length(parameters)
        obj_fun(i) = param_mat(i*2,i) - param_mat(i*2 - 1,i);
end
obj_fun  = sum(obj_fun);
obj_LLP  = obj_fun;

% Define order of parameter variables
for i = 1:1:length(parameters)
    lo_vars(i*2*length(parameters) - 2*length(parameters) + 1 :i*2*length(parameters))=param_mat(:,i);
end

% Create bounds for parameters
for i = 1:1:length(parameters)
    L_bounds(length(u) + 1 + 2*length(parameters)*(i-1):length(u) + i*2*length(parameters))...
        = ones(1,2*length(parameters))*lower_bounds(i + 1);
    U_bounds(length(u) + 1 + 2*length(parameters)*(i-1):length(u) + i*2*length(parameters))...
        = ones(1,2*length(parameters))*upper_bounds(i + 1);
end

% add bounds for control vector
L_bounds(1:length(u)) = ones(1,length(u))*lower_bounds(1);
U_bounds(1:length(u)) = ones(1,length(u))*upper_bounds(1);
end

