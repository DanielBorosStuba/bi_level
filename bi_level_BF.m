function [J_opt, lv_opt, uv_opt, Log] =  bi_level_BF(obj_fun, obj_LLP, const, uv, lv, lb, ub, x0, varargin)
% BI_LEVEL_BF solves bi-level optimization problem utilizing BARON solver
%             and Blankenship & Falks algorithm.
%
%   Calling form:
%
%       [J_opt, lv_opt, uv_opt, Log] = ...
%       bi_level_BF(obj_fun, obj_LLP, const, uv, lv, lb, ub, x0, varargin)
%
%   Possible additional arguments ['MaxIter','Tol','Baronset']
%
%   Program assumes following form of optimization problem:
%       min obj_fun(x,y)
%        x
%           max obj_LLP(x,y)
%            y
%            s.t. const(x,y)<=0
%
%   Where
%
%   obj_fun is objective function for upper level problem,
%           defined in symbolic toolbox.
%   obj_LLP is objective function for lower level problem,
%           defined in symbolic toolbox.
%   const is a column vector of LLP constraints.
%
%   See also bi_level baronset baron

% validate inputs
ValidSym    = @(x) isscalar(x) && class(x)=="sym";
ValidSymCol = @(x) iscolumn(x) && class(x)=="sym";
ValidCol    = @(x) iscolumn(x) && isnumeric(x);
ValidRow    = @(x) isrow(x)    && isnumeric(x);
ValidNumber = @(x) isscalar(x) && isnumeric(x);
p = inputParser;
% define required inputs
addRequired(p,'obj_fun',ValidSym);
addRequired(p,'obj_LLP',ValidSym);
addRequired(p,'const',ValidSymCol);
addRequired(p,'uv',ValidSymCol);
addRequired(p,'lv',ValidSymCol);
addRequired(p,'lb',ValidRow);
addRequired(p,'ub',ValidRow);
addRequired(p,'x0',ValidCol);
% define optinal inputs
addOptional(p,'MaxIter',50,ValidNumber);
addOptional(p,'Tol',1e-4,ValidNumber);
addOptional(p,'BarRuntime',[300],ValidRow)
addOptional(p,'Baronset',baronset);
% parse
parse(p,obj_fun,obj_LLP,const,uv,lv,lb,ub,x0,varargin{:});


% Baronset
Baronset = p.Results.Baronset;
% Objective functions and constraints
nlcon_ULP_sym  = [];
obj_ULP_handle = matlabFunction(subs(p.Results.obj_fun,p.Results.lv,p.Results.x0),'Vars',{p.Results.uv});

% lower and upper bounds for upper level problem
lb_ULP = p.Results.lb(1:length(p.Results.uv));
ub_ULP = p.Results.ub(1:length(p.Results.uv));

% lower and upper bounds for lower level problem
lb_LLP = p.Results.lb(length(p.Results.uv)+1:end);
ub_LLP = p.Results.ub(length(p.Results.uv)+1:end);

% lower and upper bounds for LLP constraints
cl_LLP = ones(length(p.Results.const),1)*-inf;
cu_LLP = zeros(length(p.Results.const),1);

% Cycle
for i=1:1:p.Results.MaxIter
    
    % Manage Baron runtime limit
    if length(p.Results.BarRuntime) == p.Results.MaxIter
        Baronset.maxtime = p.Results.BarRuntime(n);
    end
    % Create constraints for upper level problem
    if i==1
        nlcon_ULP_sym  = [nlcon_ULP_sym; subs(p.Results.obj_LLP,...
            p.Results.lv,p.Results.x0)];
        nlcon_ULP      = matlabFunction(nlcon_ULP_sym,'Vars',{[p.Results.uv]});
    else
        nlcon_ULP_sym  = [nlcon_ULP_sym; subs(p.Results.obj_LLP,...
            p.Results.lv,vars_LLP)];
        nlcon_ULP      = matlabFunction(nlcon_ULP_sym,'Vars',{[p.Results.uv]});
    end

    % lower and upper bounds for LLP constraints
    cl_ULP = ones(length(nlcon_ULP_sym),1)*-inf;
    cu_ULP = zeros(length(nlcon_ULP_sym),1);

    %solve upper level problem
    [vars_ULP,fval_ULP,exitflag_ULP] = sbaron('fun',obj_ULP_handle,...
        'nlcon',nlcon_ULP,'cl',cl_ULP,'cu',cu_ULP,'lb',lb_ULP,'ub',...
        ub_ULP,'opts',Baronset);

    obj_LLP_handle = matlabFunction(-1*subs(p.Results.obj_LLP,...
        p.Results.uv,vars_ULP),'Vars',{[p.Results.lv]});
    sol_check = matlabFunction(subs(p.Results.obj_LLP,p.Results.uv,...
        vars_ULP),'Vars',{[p.Results.lv]});
    nlcon_LLP = matlabFunction(subs(p.Results.const,p.Results.uv,...
        vars_ULP),'Vars',{[p.Results.lv]});

    %solve lower level problem
    [vars_LLP,fval_LLP,exitflag_LLP] = sbaron('fun',obj_LLP_handle,...
        'nlcon',nlcon_LLP,'cl',cl_LLP,'cu',cu_LLP,'lb',lb_LLP,'ub',...
        ub_LLP,'opts',Baronset);

    % optimality condition
    if sol_check(vars_LLP)<=p.Results.Tol
        J_opt=fval_ULP;
        lv_opt=vars_LLP;
        uv_opt=vars_ULP;
        Log=struct('exitflag_LLP',exitflag_LLP,'exitflag_GSIP',exitflag_ULP,'LastIter',i);
        return
    elseif i==p.Results.MaxIter
        J_opt=fval_ULP;
        lv_opt=vars_LLP;
        uv_opt=vars_ULP;
        Log=struct('exitflag_LLP',exitflag_LLP,'exitflag_GSIP',exitflag_ULP,'LastIter',i);
        return
    end
end
end



