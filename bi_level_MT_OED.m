function [J_opt,U_opt,Param_opt,Log]=bi_level_MT_OED(model_x,model_y,u,parameters,parameters_values,x0,x0_value,lb,ub,start,model_type,varargin)
% BI_LEVEL_MT_OED solves problem of optimal experiment design using
%                 Mitsos and Tsoukalas algorithm(MT).
%
%   Calling form:
%
%       [J_opt, U_opt, Param_opt, Log] = bi_level_MT_OED(model_x, model_y, u,...
%       parameters, parameters_values, x0, x0_value, lb, ub, start,...
%       model_type, varargin)
%
%   Possible additional arguments
%       ['MaxIter','Alpha','Sigma','Tol','Xi','Gl_max','Gu_max','Baronset']
%
%   See also bi_level_OED baronset baron

% Prepare to validation functions
ValidSym       = @(x) isscalar(x) && class(x)=="sym";
ValidSymCol    = @(x) iscolumn(x) && class(x)=="sym";
ValidSymRow    = @(x) isrow(x)    && class(x)=="sym";
ValidMat       = @(x) ismatrix(x) && isnumeric(x);
ValidRow       = @(x) isrow(x)    && isnumeric(x);
ValidNumber    = @(x) isscalar(x) && isnumeric(x);
ValidModelType = @(x) lower(x) == "static" | lower(x)=="discrete";

p = inputParser;
% Define required inputs
addRequired(p,'model_x',ValidSym);
addRequired(p,'model_y',ValidSym);
addRequired(p,'u',ValidSymCol);
addRequired(p,'parameters',ValidSymRow);
addRequired(p,'parameters_values',ValidRow)
addRequired(p,'x0',ValidSym);
addRequired(p,'x0_value',ValidNumber);
addRequired(p,'lb',ValidRow);
addRequired(p,'ub',ValidRow);
addRequired(p,'start',ValidMat);
addRequired(p,'model_type',ValidModelType);

% Define optinal inputs
addOptional(p,'MaxIter',50,ValidNumber);
addOptional(p,'Alpha',0.95,ValidNumber);
addOptional(p,'Sigma',0.1,ValidNumber);
addOptional(p,'Tol',1e-3,ValidNumber);
addOptional(p,'Xi',[0.7],ValidRow);
addOptional(p,'Gl_max',30,ValidNumber);
addOptional(p,'Gu_max',35,ValidNumber);
addOptional(p,'BarRuntime',[300],ValidRow)
addOptional(p,'Baronset',baronset);
% Parse
parse(p,model_x,model_y,u,parameters,parameters_values,x0,x0_value,lb,ub,start,model_type,varargin{:});

% Baronset 
Baronset = p.Results.Baronset;
% Manage Xi values in iteration
if length(p.Results.Xi) == p.Results.MaxIter
    Xi = p.Results.Xi;
else
    Xi = linspace(0.5,0.9,p.Results.MaxIter);
end

% Prepare for cycle
[~,obj_LLP,const,L_bounds,U_bounds,lo_vars,~]=...
    OED_to_bilevel(p.Results.model_x,p.Results.model_y,p.Results.u,p.Results.parameters,p.Results.parameters_values,p.Results.x0,p.Results.x0_value,p.Results.lb,p.Results.ub,p.Results.Alpha,p.Results.Sigma,p.Results.model_type);

% Set auxiliary variables
syms GSIP_mu aux_var;
ep_f = p.Results.Tol;

% Prepare logs
param_LOG       = zeros(length(lo_vars),p.Results.MaxIter+1);
control_LOG     = zeros(length(p.Results.u),p.Results.MaxIter);
LLP_LOG         = zeros(length(lo_vars),p.Results.MaxIter);
AUX_LOG         = zeros(length(lo_vars)+1,p.Results.MaxIter);
param_LOG(:,1)  = reshape(p.Results.start,1,2*length(p.Results.parameters)^2);

% Initialize constraits
nlcon_GSIP = [];
const_LBD = [const;obj_LLP - GSIP_mu];
LLP_lb = L_bounds(length(p.Results.u)+1:end);
LLP_ub = U_bounds(length(p.Results.u)+1:end);

% Initialize variables for end conditions;
F_LBD  = -inf;
F_UBD  = inf;

% Define binary variables
binary = sym('z',[p.Results.MaxIter*(length(const)+1) 1]);

% Define function handles
Obj_GSIP_handle    = matlabFunction(GSIP_mu,'Vars',{[p.Results.u;GSIP_mu]});
Obj_LLP_handle     = matlabFunction(-1*obj_LLP,'Vars',{lo_vars});
Obj_LLP_AUX_handle = matlabFunction(aux_var,'Vars',{[lo_vars; aux_var]});

% Set iteration counter to 0
n = 0;

% Set position of best solution to zero
best_sol_n = 0;

% Cycle
while F_UBD - F_LBD > ep_f && p.Results.MaxIter > n
    % Iteration counter
    n = n+1;
    
    % Manage Baron runtime limit
    if length(p.Results.BarRuntime) == p.Results.MaxIter
        Baronset.maxtime = p.Results.BarRuntime(n);
    end
    
    % GSIP - LBD
    % Bounds
    GSIP_lb = [p.Results.lb(1)*ones(1,length(p.Results.u)), -inf , zeros(1,(length(const)+1)*n) ];
    GSIP_ub = [p.Results.ub(1)*ones(1,length(p.Results.u)),  inf ,  ones(1,(length(const)+1)*n) ];

    % Define binary and continuous variables
    xtype = strcat('C'*ones(1,length(p.Results.u)+1),'B'*ones(1,(length(const)+1)*n));

    % Define nlcon for GSIP
    nlcon_GSIP = [ nlcon_GSIP
        binary((length(const)+1)*(n-1)+1:n*(length(const)+1)) - 1 - const_LBD./[ones(length(const),1)*p.Results.Gl_max;-p.Results.Gu_max]
        1 - sum(binary((length(const)+1)*(n-1)+1:n*(length(const)+1)))];
    nlcon_GSIP = subs(nlcon_GSIP,lo_vars,param_LOG(:,n));

    % Create function handle from nlcon for GSIP
    GSIP_con = matlabFunction(nlcon_GSIP,'Vars',{[p.Results.u;GSIP_mu;binary(1:length(nlcon_GSIP)-n*1)]});

    % Solve GSIP
    [x,~,~] = sbaron('fun',Obj_GSIP_handle,'nlcon',GSIP_con,'cl',ones((length(const)+2)*n,1)*-inf,'cu',zeros((length(const)+2)*n,1),...
        'lb',GSIP_lb,'ub',GSIP_ub,'xtype',xtype,'opts',Baronset);
    mu_result = x(length(p.Results.u)+1);

    % Update LBD end conditon
    F_LBD = mu_result;

    % Log results
    control_LOG(:,n)=x(1:length(p.Results.u));

    % LLP
    % Update nlcon for LLP
    LLP_con = matlabFunction(subs(const,p.Results.u,control_LOG(1:length(p.Results.u),n)),'Vars',{lo_vars});

    % Solve lower level problem
    [LLP_LOG(:,n),fval,~] = sbaron('fun',Obj_LLP_handle,'nlcon',LLP_con,'cl',-inf*ones(length(const),1),'cu',zeros(length(const),1),...
        'lb',LLP_lb,'ub',LLP_ub,'opts',Baronset);

    % Check if solution is better then previous
    if  F_UBD > -fval
        F_UBD = -fval;
        best_sol_n = n;
    end

    % If interior points are found add them to param_LOG to use in next
    % iteration
    if all(LLP_con(LLP_LOG(:,n))<-ones(4,1)*1e-6)
        param_LOG(:,n+1) = LLP_LOG(:,n);
    % If interior points are not found solve LLP - AUX
    else
        % Prepare LLP_AUX_con
        LLP_AUX_con = matlabFunction([subs(const,p.Results.u,control_LOG(1:length(p.Results.u),n)) - aux_var
            Xi(n)*-fval-obj_LLP],'Vars',{[lo_vars;aux_var]});

        % Solve LLP-AUX
        [AUX_LOG(:,n),~,~] = sbaron('fun',Obj_LLP_AUX_handle,'nlcon',LLP_AUX_con,'cl',-inf*ones(length(const)+1,1),'cu',zeros(length(const)+1,1),...
            'lb',[LLP_lb,-inf],'ub',[LLP_ub,inf],'opts',Baronset);
        param_LOG(:,n+1) = AUX_LOG(1:end-1,n);
    end
end

% Prepare results for user
Q         = reshape(LLP_LOG(:,best_sol_n),2*length(p.Results.parameters),length(p.Results.parameters));
Param_opt = [];
for i=1:1:length(p.Results.parameters)
    Param_opt = [Param_opt; Q((2*i-1):2*i,i)];
end
Param_opt = reshape(Param_opt,2,length(p.Results.parameters));
Log       = struct('LastIter',n,'control',control_LOG,'LLP',LLP_LOG, 'AUX',AUX_LOG);
J_opt     = F_UBD;
U_opt     = control_LOG(:,best_sol_n);
end

