function [J_opt, lv_opt, uv_opt, Log] = bi_level_KKT(obj_fun, obj_LLP, const, uv, lv, lb, ub, varargin)
% BI_LEVEL_KKT solves bi-level optimization problem utilizing BARON solver and
%              using reformulation of lower level problem trough KKT-conditions
%
%   Calling form:
%
%       [J_opt, lv_opt, uv_opt, Log] = ...
%		bi_level_KKT(obj_fun, obj_LLP, const, uv, lv, lb, ub, varargin)
%
%   Possible additional arguments: ['Baronset']
%
%   Program assumes following form of optimization problem:
%       min obj_fun(x,y)
%        x
%           max obj_LLP(x,y)
%            y
%            s.t. const(x,y)<=0
%
%   Where
%
%   obj_fun is objective function for upper level problem,
%           defined in symbolic toolbox.
%   obj_LLP is objective function for lower level problem,
%           defined in symbolic toolbox.
%	const is a column vector of LLP constraints.
%
%   Example:
%   syms x1 x2 y1
%   obj_fun = (x1 - 0.25)^2+x2
%   obj_LLP = y + x2
%
%   const are constraints for lower level problem in the form of symbolic
%   column-wise vector
%
%   Example:
%   syms x1 x2 y1
%   cons = [ y1^2 - x2]
%
%   uv and lv define optimization variables for upper level
%   problem and lower level problem
%
%   Example:
%   syms x1 x2 y1 y2 y3
%   uv = [x1;x2]
%   lv = [y1]
%
%   lb and ub define bounds for optimized variables.
%   Bounds need to be defined in the following order of variables:
%
%   lb = [uv(1),uv(2),...,uv(n),lv(1),lv(2),...,lv(m)]
%   ub = [uv(1),uv(2),...,uv(n),lv(1),lv(2),...,lv(m)]
%
%   Example:
%   syms x1 x2 y1
%   uv = [x1;x2]
%   lv = [y1]
%
%   and bounds are
%
%   x1 = [-1,1]
%   x2 = [-1.1,1.1]
%   y1 = [-0.9,0.9]
%
%   then
%
%   lb = [-1,-1.1,-0.9]
%   ub = [1,1.1,0.9]
%
%   See also bi_level baronset baron


% validate inputs
ValidSym    = @(x) isscalar(x) && class(x)=="sym";
ValidSymCol = @(x) iscolumn(x) && class(x)=="sym";
ValidRow    = @(x) isrow(x)    && isnumeric(x);
p = inputParser;

% define required inputs
addRequired(p,'obj_fun',ValidSym);
addRequired(p,'obj_LLP',ValidSym);
addRequired(p,'const',ValidSymCol);
addRequired(p,'uv',ValidSymCol);
addRequired(p,'lv',ValidSymCol);
addRequired(p,'lb',ValidRow);
addRequired(p,'ub',ValidRow);

% define optinal inputs
addOptional(p,'Baronset',baronset);

% parse
parse(p,obj_fun,obj_LLP,const,uv,lv,lb,ub,varargin{:});

% Create order of variables, constraints and objective function
[~,obj,nlcon,cl,cu] = CreateNlconKKT(p.Results.obj_fun,p.Results.obj_LLP,p.Results.const,p.Results.uv,p.Results.lv);

% Include constraints for KKT multipliers
lb=[p.Results.lb -inf*ones(1,length(p.Results.const))];
ub=[p.Results.ub inf*ones(1,length(p.Results.const))];

% Solve defined problem
[x,J_opt,exitflag] = sbaron('fun',obj,'nlcon',nlcon,'cl',cl,'cu',cu,...
    'lb',lb,'ub',ub,'opts',p.Results.Baronset);

uv_opt = x(1:length(p.Results.uv));
lv_opt = x(length(p.Results.uv)+1:(length(p.Results.lv)+length(p.Results.uv)));
Log    = struct('Lambdas',x((length(p.Results.lv)+length(p.Results.uv))+1:end),'exitflag',exitflag);

end