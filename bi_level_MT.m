function [J_opt,lv_opt,uv_opt,Log] = bi_level_MT(obj_fun,obj_LLP,const,uv,lv,lb,ub,start,varargin)
% BI_LEVEL_Bf solves bi-level optimization problem utilizing BARON solver and
%             Mitsos & Tsoukalas algorithm.
%
%   Calling form:
%
%       [J_opt, lv_opt, uv_opt, Log] = ...
%       bi_level_BF(obj_fun, obj_LLP, const, uv, lv, lb, ub, x0, varargin)
%
%   Possible additional arguments:
%    ['MaxIter','Tol','Xi','Gl_max','Gu_max','Eps_L','Eps_U','Eps_R','Baronset']
%
%   Program assumes following form of optimization problem:
%       min obj_fun(x,y)
%        x
%           max obj_LLP(x,y)
%            y
%            s.t. const(x,y)<=0
%
%   Where
%
%   obj_fun is objective function for upper level problem,
%           defined in symbolic toolbox.
%   obj_LLP is objective function for lower level problem,
%           defined in symbolic toolbox.
%   const is a column vector of LLP constraints.
%
%   See also bi_level baronset baron

% Prepare to validation functions
ValidSym       = @(x) isscalar(x) && class(x)=="sym";
ValidSymCol    = @(x) iscolumn(x) && class(x)=="sym";
ValidCol       = @(x) iscolumn(x) && isnumeric(x);
ValidRow       = @(x) isrow(x)    && isnumeric(x);
ValidNumber    = @(x) isscalar(x) && isnumeric(x);

p = inputParser;
% Define required inputs
addRequired(p,'obj_fun',ValidSym);
addRequired(p,'obj_LLP',ValidSym);
addRequired(p,'const',ValidSymCol);
addRequired(p,'uv',ValidSymCol);
addRequired(p,'lv',ValidSymCol);
addRequired(p,'lb',ValidRow);
addRequired(p,'ub',ValidRow);
addRequired(p,'start',ValidCol);

% Define optinal inputs
addOptional(p,'MaxIter',50,ValidNumber);
addOptional(p,'Tol',1e-3,ValidNumber);
addOptional(p,'Xi',[0.65],ValidRow);
addOptional(p,'Gl_max',30,ValidNumber);
addOptional(p,'Gu_max',35,ValidNumber);
addOptional(p,'Eps_L',2,ValidNumber);
addOptional(p,'Eps_U',2,ValidNumber);
addOptional(p,'Eps_R',2,ValidNumber);
addOptional(p,'BarRuntime',[300],ValidRow)
addOptional(p,'Baronset',baronset);
% Parse
parse(p,obj_fun,obj_LLP,const,uv,lv,lb,ub,start,varargin{:});

% Set auxiliary variables
syms aux_var eps_L eps_U;
Baronset = p.Results.Baronset;
% Manage Xi values in iteration
if length(p.Results.Xi) == p.Results.MaxIter
    Xi = p.Results.Xi;
else
    Xi = linspace(0.5,0.9,p.Results.MaxIter);
end

% Prepare logs lbd
lv_LOG_LBD      = zeros(length(p.Results.lv),p.Results.MaxIter+1);
uv_LOG_LBD      = zeros(length(p.Results.uv),p.Results.MaxIter);
LLP_LOG_LBD     = zeros(length(p.Results.lv),p.Results.MaxIter);
AUX_LOG_LBD     = zeros(length(p.Results.lv)+1,p.Results.MaxIter);
lv_LOG_LBD(:,1) = p.Results.start;

% Prepare log UBD
lv_LOG_UBD      = zeros(length(p.Results.lv),p.Results.MaxIter+1);
uv_LOG_UBD      = zeros(length(p.Results.uv),p.Results.MaxIter);
LLP_LOG_UBD     = zeros(length(p.Results.lv),p.Results.MaxIter);
lv_LOG_UBD(:,1) = p.Results.start;

% Initialize constraits
nlcon_GSIP_LBD = [];
nlcon_GSIP_UBD = [];
const_LBD      = [const;obj_LLP];
const_UBD      = [(const-ones(length(const),1)*eps_L);...
                  -1*(obj_LLP + eps_U)];
LLP_lb         = p.Results.lb(length(p.Results.uv)+1:end);
LLP_ub         = p.Results.ub(length(p.Results.uv)+1:end);

% Initialize variables for end conditions;
F_LBD  = -inf;
F_UBD  = inf;

% Define binary variables
binary_LBD = sym('z',[p.Results.MaxIter*(length(p.Results.const)+1) 1]);
binary_UBD = sym('z',[p.Results.MaxIter*(length(p.Results.const)+1) 1]);

% Define function handles
Obj_GSIP_handle    = matlabFunction(subs(p.Results.obj_fun,p.Results.lv,p.Results.start),'Vars',...
                                    {[p.Results.uv]});
Obj_LLP_AUX_handle = matlabFunction(aux_var,'Vars',...
                                    {[p.Results.lv; aux_var]});

% Set iteration counter to 0
n = 0;

% Set position of best solution to zero
r = 1;
% Cycle
while  F_UBD - F_LBD > p.Results.Tol &&  p.Results.MaxIter > n
    % Iteration counter
    n = n+1;
    
    % Manage Baron runtime limit
    if length(p.Results.BarRuntime) == p.Results.MaxIter
        Baronset.maxtime = p.Results.BarRuntime(n);
    end
    
    % GSIP - LBD
    % Bounds
    GSIP_lb_LBD = [p.Results.lb(1,1:length(p.Results.uv)),...
                   zeros(1,(length(p.Results.const)+1)*n) ];
    GSIP_ub_LBD = [p.Results.ub(1,1:length(p.Results.uv)),...
                   ones(1,(length(p.Results.const)+1)*n) ];

    % Define binary and continuous variables
    xtype_LBD = strcat('C'*ones(1,length(p.Results.uv)),...
                       'B'*ones(1,(length(p.Results.const)+1)*n));

    % Define nlcon for GSIP
    nlcon_GSIP_LBD = [ nlcon_GSIP_LBD
                       binary_LBD((length(p.Results.const)+1)*(n-1)+1:n*(length(p.Results.const)+1)) - 1 - const_LBD./[ones(length(p.Results.const),1)*p.Results.Gl_max;-1*p.Results.Gu_max]
                       1 - sum(binary_LBD((length(p.Results.const)+1)*(n-1)+1:n*(length(p.Results.const)+1)))];

    nlcon_GSIP_LBD = subs(nlcon_GSIP_LBD,p.Results.lv,lv_LOG_LBD(:,n));

    % Create function handle from nlcon for GSIP
    GSIP_con_LBD = matlabFunction(nlcon_GSIP_LBD,'Vars',{[p.Results.uv;binary_LBD(1:length(nlcon_GSIP_LBD)-n*1)]});

    % Solve GSIP LBD
    [x,fval,~] = sbaron('fun',Obj_GSIP_handle,'nlcon',GSIP_con_LBD,'cl',ones((length(p.Results.const)+2)*n,1)*-inf,'cu',zeros((length(p.Results.const)+2)*n,1),...
        'lb',GSIP_lb_LBD,'ub',GSIP_ub_LBD,'xtype',xtype_LBD,'opts',Baronset);

    % Update end conditions
    F_LBD = fval;

    % Log results
    uv_LOG_LBD(:,n)=x(1:length(p.Results.uv));

    % LLP
    % Update nlcon for LLP
    LLP_con_LBD = matlabFunction(subs(p.Results.const,p.Results.uv,uv_LOG_LBD(1:length(p.Results.uv),n)),'Vars',{p.Results.lv});

    % Update objective function
    Obj_LLP_LBD_handle = matlabFunction(subs(-1*p.Results.obj_LLP,p.Results.uv,uv_LOG_LBD(:,n)),'Vars',{p.Results.lv});

    % Solve lower level problem
    [LLP_LOG_LBD(:,n),fval,~] = sbaron('fun',Obj_LLP_LBD_handle,'nlcon',LLP_con_LBD,'cl',-inf*ones(length(p.Results.const),1),'cu',zeros(length(p.Results.const),1),...
        'lb',LLP_lb,'ub',LLP_ub,'opts',Baronset);


    % If interior points are found add them to param_LOG to use in next
    % iteration
    if all(LLP_con_LBD(LLP_LOG_LBD(:,n))<-ones(length(p.Results.const),1)*1e-6)
        lv_LOG_LBD(:,n+1) = LLP_LOG_LBD(:,n);
    % If interior points are not found solve LLP - AUX
    else
        % Prepare LLP_AUX_con
        LLP_AUX_con = matlabFunction([subs(p.Results.const,p.Results.uv,uv_LOG_LBD(1:length(p.Results.uv),n)) - aux_var
            Xi(n)*-fval-subs(obj_LLP,p.Results.uv,uv_LOG_LBD(:,n))],'Vars',{[p.Results.lv;aux_var]});

        % Solve LLP-AUX
        [AUX_LOG_LBD(:,n),~,~] = sbaron('fun',Obj_LLP_AUX_handle,'nlcon',LLP_AUX_con,'cl',-inf*ones(length(p.Results.const)+1,1),'cu',zeros(length(p.Results.const)+1,1),...
            'lb',[LLP_lb,-inf],'ub',[LLP_ub,inf],'opts',Baronset);
        lv_LOG_LBD(:,n+1) = AUX_LOG_LBD(1:end-1,n);
    end
    % Solve GSIP - UBD
    % Bounds
    GSIP_lb_UBD = [p.Results.lb(1,1:length(p.Results.uv)),p.Results.Eps_L/r,p.Results.Eps_U/r, zeros(1,(length(p.Results.const)+1)*n) ];
    GSIP_ub_UBD = [p.Results.ub(1,1:length(p.Results.uv)),p.Results.Eps_L/r,p.Results.Eps_U/r, ones(1,(length(p.Results.const)+1)*n) ];

    % Define binary and continuous variables
    xtype_UBD = strcat('C'*ones(1,length(p.Results.uv)+2),'B'*ones(1,(length(p.Results.const)+1)*n));

    % Define nlcon for GSIP
    nlcon_GSIP_UBD = [ nlcon_GSIP_UBD
        binary_UBD((length(p.Results.const)+1)*(n-1)+1:n*(length(p.Results.const)+1)) - 1 - const_UBD./[ones(length(p.Results.const),1)*p.Results.Gl_max;p.Results.Gu_max]
        1 - sum(binary_UBD((length(p.Results.const)+1)*(n-1)+1:n*(length(p.Results.const)+1)))];

    nlcon_GSIP_UBD = subs(nlcon_GSIP_UBD,p.Results.lv,lv_LOG_UBD(:,n));

    % Create function handle from nlcon for GSIP
    GSIP_con_UBD = matlabFunction(nlcon_GSIP_UBD,'Vars',{[p.Results.uv;eps_L;eps_U;binary_UBD(1:length(nlcon_GSIP_UBD)-n*1)]});

    % Solve GSIP UBD
    [x,fval_UBD,~] = sbaron('fun',Obj_GSIP_handle,'nlcon',GSIP_con_UBD,'cl',ones((length(p.Results.const)+2)*n,1)*-inf,'cu',zeros((length(p.Results.const)+2)*n,1),...
        'lb',GSIP_lb_UBD,'ub',GSIP_ub_UBD,'xtype',xtype_UBD,'opts',Baronset);

    % Log results
    uv_LOG_UBD(:,n)=x(1:length(p.Results.uv));

    % LLP
    % Update nlcon for LLP
    LLP_con_UBD = matlabFunction(subs(p.Results.const,p.Results.uv,uv_LOG_UBD(1:length(p.Results.uv),n)),'Vars',{p.Results.lv});

    % Update objective function
    Obj_LLP_UBD_handle = matlabFunction(subs(-1*p.Results.obj_LLP,p.Results.uv,uv_LOG_UBD(:,n)),'Vars',{p.Results.lv});

    % Solve lower level problem
    [LLP_LOG_UBD(:,n),fval,~] = sbaron('fun',Obj_LLP_UBD_handle,'nlcon',LLP_con_UBD,'cl',-inf*ones(length(p.Results.const),1),'cu',zeros(length(p.Results.const),1),...
        'lb',LLP_lb,'ub',LLP_ub,'opts',Baronset);

    % Prepare Log for next iteration
    lv_LOG_UBD(:,n+1) = LLP_LOG_UBD(:,n);
    if -fval <= 1e-4
        if fval_UBD <= F_UBD
            F_UBD = fval_UBD;
        end
        r = r*p.Results.Eps_R;
    end
end
J_opt  = F_LBD;
lv_opt = LLP_LOG_LBD(:,n);
uv_opt = uv_LOG_LBD(:,n) ;
Log    = struct('LastIter',n,...
                'UBD',struct('LV',lv_LOG_UBD ,'UV',uv_LOG_UBD,'LLP',LLP_LOG_UBD),...
                'LBD',struct('LV',lv_LOG_LBD ,'UV',uv_LOG_LBD,'LLP',LLP_LOG_LBD,'AUX',AUX_LOG_LBD));
end




