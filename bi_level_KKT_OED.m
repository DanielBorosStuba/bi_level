function [J_opt,U_opt,Param_opt,Log]=bi_level_KKT_OED(model_x,model_y,u,parameters,parameters_values,x0,x0_value,lb,ub,ModelType,varargin)
% BI_LEVEL_BF solves bi-level optimization problem utilizing BARON solver
%             and Blankenship & Falks algorithm.
%
%   Calling form:
%
%       [J_opt, U_opt, Param_opt, Log] = bi_level_KKT_OED(model_x, model_y, ...
%		u, parameters, parameters_values, x0, x0_value, lb, ub, ModelType, varargin)
%
%   Possible additional arguments ['Alpha','Sigma','Baronset']
%
%   See also bi_level_OED baronset baron

% Prepare to validation functions
ValidSym       = @(x) isscalar(x) && class(x)=="sym";
ValidSymCol    = @(x) iscolumn(x) && class(x)=="sym";
ValidSymRow    = @(x) isrow(x)    && class(x)=="sym";
ValidRow       = @(x) isrow(x)    && isnumeric(x);
ValidNumber    = @(x) isscalar(x) && isnumeric(x);
ValidModelType = @(x) lower(x) == "static" | lower(x)=="discrete";

p = inputParser;
% Define required inputs
addRequired(p,'model_x',ValidSym);
addRequired(p,'model_y',ValidSym);
addRequired(p,'u',ValidSymCol);
addRequired(p,'parameters',ValidSymRow);
addRequired(p,'parameters_values',ValidRow)
addRequired(p,'x0',ValidSym);
addRequired(p,'x0_value',ValidNumber);
addRequired(p,'lb',ValidRow);
addRequired(p,'ub',ValidRow);
addRequired(p,'model_type',ValidModelType);

addOptional(p,'Alpha',0.1,ValidNumber);
addOptional(p,'Sigma',0.95,ValidNumber);
addOptional(p,'Baronset',baronset);

parse(p,model_x,model_y,u,parameters,parameters_values,x0,x0_value,lb,ub,ModelType,varargin{:});

% Reformulate to bi level problem
[obj_fun,obj_LLP,const,lb,ub,lo_vars,up_vars] = OED_to_bilevel(...
        p.Results.model_x,...
        p.Results.model_y,...
        p.Results.u,...
        p.Results.parameters,...
        p.Results.parameters_values,...
        p.Results.x0,...
        p.Results.x0_value,...
        p.Results.lb,...
        p.Results.ub,...
        p.Results.Alpha,...
        p.Results.Sigma,...
        p.Results.model_type);

% Call bi_level_KKT
[J_opt,lv_opt,U_opt,Log]= bi_level_KKT(obj_fun,obj_LLP,const,up_vars,...
                          lo_vars,lb,ub,'Baronset',p.Results.Baronset);
                      
% Prepare results for user
Q         = reshape(lv_opt,2*length(p.Results.parameters),length(p.Results.parameters));
Param_opt = zeros(2,length(p.Results.parameters));
for i     = 1 : 1 : length(p.Results.parameters)
    Param_opt(:,i) = Q((2*i-1) : 2*i,i);
end
Param_opt = reshape(Param_opt,2,length(p.Results.parameters));
Log.Params = lv_opt;
end









